import React, { useState } from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import { Container, Row, Col } from "react-bootstrap";
import Question from "../Question";
import "./styles.css";

const Quiz = ({ question }) => {
  // Local states to track question number and correct question
  const [counter, setCounter] = useState(0);
  const [correct, setCorrect] = useState(0);

  return (
    <Container fluid>
      <Row>
        <Col>
          <button className='mt-3'>
            <Link to='/'>Home</Link>
          </button>
        </Col>
      </Row>
      <Row>
        <Col className='text-center'>
          <h1>Quizz</h1>
          <Question
            question={question.questions[counter]}
            onNext={() => setCounter(counter + 1)}
            onCorrect={() => setCorrect(correct + 1)}
          />
          {counter === 10 ? (
            <h2>
              Start Again?
              <button className='start-again'>
                <Link to='/'>Start Quiz</Link>
              </button>
              <p className='mt-5'>Correct answers: {correct} </p>
            </h2>
          ) : (
            ""
          )}
        </Col>
      </Row>
    </Container>
  );
};

const mapStateToProps = (state) => ({
  question: state.question,
});

export default connect(mapStateToProps)(Quiz);
