import React, { useEffect } from "react";
import PropTypes from "prop-types";
import { getCategories } from "../../redux/actions/category";
import { connect } from "react-redux";

const CategorySelect = ({ getCategories, category, handleCategory }) => {
  // Getting API call to get Chosen category and difficulty for user
  useEffect(() => {
    getCategories();
  }, [getCategories]);

  const handleSelect = (e) => {
    handleCategory(e.target.value);
  };

  return (
    <div>
      {category ? (
        <select onClick={(e) => handleSelect(e)}>
          {category.categories.map((category, index) => (
            <option key={index} value={category.id} defaultValue=''>
              {category.name}
            </option>
          ))}
        </select>
      ) : (
        <p>"Loading"</p>
      )}
    </div>
  );
};

CategorySelect.propTypes = {
  getCategories: PropTypes.func.isRequired,
};

const mapStateToProps = (state) => ({
  category: state.category,
});

export default connect(mapStateToProps, { getCategories })(CategorySelect);
