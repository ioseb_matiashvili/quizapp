import React, { useState } from "react";
import { Link } from "react-router-dom";
import { Container, Row, Col } from "react-bootstrap";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { getQuestions } from "../../redux/actions/question";
import Select from "./CategorySelect";
import "./styles.css";

const Main = ({ getQuestions }) => {
  const [difficulty, setDifficulty] = useState("");
  const [categoryId, setCategoryId] = useState(0);

  const handleCategory = (categoryId) => {
    setCategoryId(categoryId);
  };

  return (
    <Container fluid className='text-center mt-5'>
      <Row>
        <Col>
          <h1>Quiz Appication</h1>
        </Col>
      </Row>
      <Row className='mt-4'>
        <Col>
          <h3>Choose Category</h3>
          <Select handleCategory={handleCategory} />
        </Col>

        <Col>
          <h3>Choose the Difficulty</h3>
          <select onClick={(e) => setDifficulty(e.target.value)}>
            <option value='easy'>Easy</option>
            <option value='medium'>Medium</option>
            <option value='hard'>hard</option>
          </select>
        </Col>
      </Row>

      <Row className='mt-5'>
        <Col className='text-center'>
          <button>
            <Link
              to='/app'
              onClick={() => getQuestions(categoryId, difficulty)}
            >
              Start Quiz
            </Link>
          </button>
        </Col>
      </Row>
    </Container>
  );
};

Main.propTypes = {
  getQuestions: PropTypes.func.isRequired,
};

const mapStateToProps = (state) => ({});

export default connect(mapStateToProps, { getQuestions })(Main);
