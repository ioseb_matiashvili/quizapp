import React, { useState, useEffect } from "react";
import { Container, Row, Col } from "react-bootstrap";
import "./styles.css";

const Button = ({ onClick, answer }) => {
  return (
    <button
      onClick={onClick}
      className='answer-button m-2'
      dangerouslySetInnerHTML={{ __html: answer }}
    />
  );
};

const Question = ({ question, onNext, onCorrect }) => {
  const [correctClicked, setCorrectClicked] = useState(false);
  useEffect(() => {
    setCorrectClicked(false);
  }, [question]);

  const handleCorrect = () => {
    setCorrectClicked(true);
    onCorrect();
  };

  if (!question) return null;

  return (
    <Container fluid>
      <Row>
        <Col>
          <h4 dangerouslySetInnerHTML={{ __html: question.question }} />
        </Col>
      </Row>

      {/* Checking which type of question is boolean/normal */}
      {question.type === "multiple" ? (
        <Row>
          <Col className='mt-4'>
            <Button answer={question.incorrect_answers[0]} />
            <Button answer={question.incorrect_answers[1]} />
          </Col>
          <Col className='mt-4'>
            <Button answer={question.incorrect_answers[2]} />
            <Button
              onClick={correctClicked ? null : handleCorrect}
              answer={question.correct_answer}
            />
          </Col>
        </Row>
      ) : (
        <Row>
          <Col>
            <Button
              onClick={correctClicked ? null : handleCorrect}
              answer={question.correct_answer}
            />
            <Button answer={question.incorrect_answers[0]} />
          </Col>
        </Row>
      )}

      <button className='next-button mt-3' onClick={onNext}>
        Next
      </button>
    </Container>
  );
};

export default Question;
