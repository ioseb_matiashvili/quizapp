import { GET_CATEGORIES, CATEGORIES_FAILED } from "../actions/types";
import axios from "axios";

export const getCategories = () => async (dispatch) => {
  try {
    const res = await axios.get("https://opentdb.com/api_category.php");
    dispatch({
      type: GET_CATEGORIES,
      payload: res.data.trivia_categories,
    });
  } catch (err) {
    dispatch({
      type: CATEGORIES_FAILED,
      payload: err.statusText,
    });
  }
};
