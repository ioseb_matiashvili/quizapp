import { GET_QUESTIONS, QUESTIONS_FAILED } from "../actions/types";
import axios from "axios";

// Get Questions
// getQuestions(category, difficulty)
export const getQuestions = (category, difficulty) => async (dispatch) => {
  try {
    // Checking if the category and difficulty are given
    const res = await axios.get(
      `https://opentdb.com/api.php?amount=10${
        category ? `&category=${category}` : ""
      }${difficulty ? `&difficulty=${difficulty}` : ""}`
    );
    dispatch({
      type: GET_QUESTIONS,
      payload: res.data.results,
    });
  } catch (err) {
    dispatch({
      type: QUESTIONS_FAILED,
      payload: err.statusText,
    });
  }
};
