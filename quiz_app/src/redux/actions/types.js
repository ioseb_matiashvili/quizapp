// Questions
export const GET_QUESTIONS = "GET_QUESTIONS";
export const QUESTIONS_FAILED = "QUESTIONS_FAILED";

// Categories
export const GET_CATEGORIES = "GET_CATEGORIES";
export const CATEGORIES_FAILED = "GET_CATEGORIES";
