import { combineReducers } from "redux";
import question from "./question";
import category from "./category";

// Cobine all reducers
export default combineReducers({
  question,
  category,
});
