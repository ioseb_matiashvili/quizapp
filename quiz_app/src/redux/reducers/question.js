import { GET_QUESTIONS, QUESTIONS_FAILED } from "../actions/types";

const initialState = {
  questions: [],
  loading: false,
  error: {},
};

export default function (state = initialState, action) {
  const { type, payload } = action;

  switch (type) {
    case GET_QUESTIONS:
      return {
        ...state,
        questions: payload,
        loading: false,
      };

    case QUESTIONS_FAILED:
      return {
        ...state,
        error: payload,
        loading: false,
      };

    default:
      return state;
  }
}
