import React from "react";
import "./App.css";
import Main from "./components/Main";
import Quiz from "./components/Quiz";
import { BrowserRouter as Router, Route } from "react-router-dom";

// Redux
import { Provider } from "react-redux";
import store from "./redux/store";

function App() {
  return (
    <Provider store={store}>
      <Router>
        <Route exact path='/' component={Main} />
        <Route exact path='/app' component={Quiz} />
      </Router>
    </Provider>
  );
}

export default App;
